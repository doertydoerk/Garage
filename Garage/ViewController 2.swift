//
//  ViewController.swift
//  Garage
//
//  Created by Dirk Gerretz on 01/01/16.
//  Copyright © 2016 [code2app];. All rights reserved.
//

import Alamofire
import AudioToolbox
import UIKit

class ViewController: UIViewController, C2AApiClientDelegate {
    // MARK: - Properties

    let apiClient = C2AApiClient()
    @IBOutlet var progressBar: UIProgressView!
    @IBOutlet var infoView: UILabel!
    @IBOutlet var doorButton: UIButton!
    @IBOutlet var doorButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet var environmentLabel: UILabel!
    let defaultColor = UIColor(red: 0.99, green: 0.57, blue: 0.20, alpha: 1.00)

    // availibility of the server will automatically set
    // button to enabled/disabled. Default is disabled
    public var serverIsAvailable = false {
        didSet {
            switch serverIsAvailable {
            case false:
                print("Server's not responding")
                doorButton.isEnabled = false // should be false unless for testing
                doorButton.setTitleColor(UIColor.lightGray, for: UIControlState())
                // If this request fails 'serverIsAvailable' will be set to false again.
                // Hence, unless the request comes back with 'SUCCESS' this check will loop.
                apiClient.getStatus()

            default:
                print("Server Available")
                doorButton.isEnabled = true
                doorButton.setTitleColor(UIColor(red: 0.93, green: 0.54, blue: 0.07, alpha: 1), for: UIControlState())
                // apiClient.getStatus() //this is not redundant to the same line in case false. This will only call getStatus() once.
            }
        }
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setBackgroundGradient()
        apiClient.delegate = self

        // adjust UI for iPhone4 screen
        if UIScreen.main.bounds.size.height <= 480.0 {
            doorButtonTopConstraint.constant = 20.0
        }

        // assign self to app delegate for background notifications
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.mainViewController = self

        // make button respond only to long taps to avoid accidentally opening the door
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(toggleDoor(_:)))
        doorButton.addGestureRecognizer(longTap)

        #if Latest
            environmentLabel.text = "Latest"
        #else
            environmentLabel.text = ""
        #endif

        resetProgressBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        infoView.text = "Waiting for server ..."
    }

    // MARK: - Actions

    @IBAction func doorButtonPressed(_: C2ARoundButton) {
        lunchProgressBar(duration: 0.5)
    }

    func toggleDoor(_ sender: UIGestureRecognizer) {
        if sender.state == .began {
            // vibrate phone
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

            // open door
            apiClient.postToggleDoor()

            resetProgressBar()
        }
    }

    @IBAction func testPressed(_: Any) {
        apiClient.postTest()
    }

    func lunchProgressBar(duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: { () in
            self.progressBar.setProgress(1.0, animated: true)
        })
    }

    func resetProgressBar() {
        progressBar.setProgress(0.0, animated: false)
    }

    func setBackgroundGradient() {
        let colors = C2AGradient()
        view.backgroundColor = UIColor.clear
        let backgroundLayer = colors.gl
        backgroundLayer.frame = view.frame
        view.layer.insertSublayer(backgroundLayer, at: 0)
    }

    func didReceiveNotification(message: String?) {
        let alert = UIAlertController(title: "Garage Door Open", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        alert.view.tintColor = defaultColor
        present(alert, animated: true, completion: nil)
    }

    // MARK: - API Client Delegate

    func didCompleteRequest(response: String, json: [String: Any]?) {
        if response == "SUCCESS" {
            serverIsAvailable = true

            if let json = json {
                if json["Notification"] != nil { return }

                let isOpen = json["isOpen"] as! Bool

                if isOpen {
                    infoView.text = "is open"
                } else {
                    infoView.text = "is closed"
                }
            }

        } else {
            serverIsAvailable = false
            infoView.text = "Server's not responding"
        }
    }

    // Perform frequent statu checks
    func checkDoorState() {
        DispatchQueue.global(qos: .background).async {
            // break out of loop when app enter background
            if UIApplication.shared.applicationState == .background { return }

            if UIApplication.shared.applicationState == .active,
               self.serverIsAvailable == true
            {
                print("Door Status Check...")
                self.apiClient.getStatus()
            } else {
                print("Skipping Door Status Check...")
            }
            sleep(10)
            self.checkDoorState()
        }
    }
}
