import UIKit

class C2AGradient {
    let colorTop = UIColor(red: 190 / 255, green: 190 / 255, blue: 190 / 255, alpha: 1.0).cgColor
    let colorBottom = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1.0).cgColor

    let gl: CAGradientLayer

    init() {
        gl = CAGradientLayer()
        gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
    }
}
