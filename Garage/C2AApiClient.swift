import Alamofire
import Foundation

protocol C2AApiClientDelegate: AnyObject {
    func didCompleteRequest(response: String, status: Status)
}

class C2AApiClient: NSObject {
    typealias ResponseHandlerJSON = (DataResponse<Any>) -> Void
    typealias ResponseHandlerPlainText = (DataResponse<String>) -> Void

    // MARK: - Properties

    weak var delegate: C2AApiClientDelegate?
    let productionHost: String = "http://garage.fritz.box:8080"
    var baseURL: String?
    let requestTimeout: TimeInterval = 10
    let deviceName: String

    private enum EndPoint: String {
        case Status = "/status", Toggle = "/toggle", Ping = "/ping", Log = "/log"
    }

    // TODO remove environment from init()
    init(environment: String, deviceName: String) {
        switch environment {
        default:
            baseURL = productionHost
        }

        self.deviceName = deviceName
    }

    // MARK: - Public Funtions

    /* Used to check on the door status but also on server availibility.
     * That is, no further requests should be made unless this returns 200,
     * in order to prevent delayed, unattended openening of the door.
     */
    func getStatus(completionHandler: @escaping ResponseHandlerJSON) {
        httpRequestAcceptJSON(method: .get, endPoint: .Status, completionHandler: { response in
            completionHandler(response)
        })
    }

    // The function is unaware of the door status. It will just push the trigger
    // of the garage door driver that will either open or close the door.
    func getToggleDoor(completionHandler: @escaping ResponseHandlerPlainText) {
        httpRequestAcceptPlainText(method: .get, endPoint: .Toggle, completionHandler: { response in
            completionHandler(response)
        })
    }

    func handleToggleResponse(response: DataResponse<String>) {
        switch response.result.isSuccess {
        case true:
            print("REQUEST SUCCESSFUL: \(String(describing: response.response?.statusCode))")
            delegate?.didCompleteRequest(response: "SUCCESS", status: Status(status: "", distance: 0, threshold: 0))
        default:
            // print(response)
            delegate?.didCompleteRequest(response: "FAILURE", status: Status(status: "", distance: 0, threshold: 0))
        }
    }

    func handleStatusResponse(response: DataResponse<Any>) {
        switch response.result.isSuccess {
        case true:
            if let data = response.data {
                do {
                    let status = try Status(data: data)
                    delegate?.didCompleteRequest(response: "SUCCESS", status: status)
                } catch {
                    // print("REQUEST FAILED: \(String(describing: response.response?.statusCode))")
                    // print(error)
                    delegate?.didCompleteRequest(response: "SUCCESS", status: Status(status: "", distance: 0, threshold: 0))
                }
            }
        default:
            // print(response)
            delegate?.didCompleteRequest(response: "FAILURE", status: Status(status: "", distance: 0, threshold: 0))
        }
    }

    // MARK: - Private Funtions

    private func httpRequestAcceptJSON(method: HTTPMethod, endPoint: EndPoint, completionHandler: @escaping ResponseHandlerJSON) {
        if let baseURL = baseURL {
            let url = baseURL + endPoint.rawValue
            var request = URLRequest(url: URL(string: url)!)
            request.timeoutInterval = requestTimeout
            request.httpMethod = method.rawValue
            request.addValue(deviceName, forHTTPHeaderField: "device_name")
            Alamofire.request(request as URLRequestConvertible as URLRequestConvertible).responseJSON(completionHandler: completionHandler)
        }
    }

    private func httpRequestAcceptPlainText(method: HTTPMethod, endPoint: EndPoint, completionHandler: @escaping ResponseHandlerPlainText) {
        if let baseURL = baseURL {
            let url = baseURL + endPoint.rawValue
            var request = URLRequest(url: URL(string: url)!)
            request.timeoutInterval = requestTimeout
            request.httpMethod = method.rawValue
            request.addValue(deviceName, forHTTPHeaderField: "device_name")
            Alamofire.request(request as URLRequestConvertible as URLRequestConvertible).responseString(completionHandler: completionHandler)
        }
    }
}
