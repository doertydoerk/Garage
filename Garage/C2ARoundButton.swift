import UIKit

@IBDesignable
class C2ARoundButton: UIButton {
    // MARK: - Prpoperties

    // MARK: - Inspectables

    @IBInspectable var diameter: CGFloat = 100 {
        didSet {
            layer.cornerRadius = diameter / 2
            layer.masksToBounds = (diameter / 2) > 0
            layer.borderWidth = diameter
            frame.size.width = diameter
            frame.size.height = diameter
            // self.frame.origin.x = diameter / 2
        }
    }

    @IBInspectable var borderWidth: CGFloat = 2 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

    @IBInspectable var imageNormal: UIImage? {
        didSet {
            setImage(imageNormal, for: .normal)
        }
    }

    @IBInspectable var imageHighlighted: UIImage? {
        didSet {
            setImage(imageHighlighted, for: .highlighted)
        }
    }
}
