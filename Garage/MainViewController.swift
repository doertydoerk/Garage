import Alamofire
import AudioToolbox
import UIKit

class MainViewController: UIViewController, C2AApiClientDelegate {
    // MARK: - Properties

    let defaults = UserDefaults.standard
    let open = "open", closed = "closed", unknown = "unknown"
    var apiClient: C2AApiClient!
    let staging = "staging"
    let production = "production"
    let environment = "environment"
    var longTap: UILongPressGestureRecognizer!
    var timer: Timer?
    let checkInterval: TimeInterval = 3

    @IBOutlet var progressBar: UIProgressView!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var doorButton: UIButton!
    @IBOutlet var doorButtonTopConstraint: NSLayoutConstraint!
    let defaultColor = UIColor(red: 0.99, green: 0.57, blue: 0.20, alpha: 1.00)

    // availibility of the server will automatically set
    // button to enabled/disabled. Default is disabled
    public var serverIsAvailable = false {
        didSet {
            switch serverIsAvailable {
            case false:
                doorButton.isEnabled = false
                longTap.isEnabled = false
                doorButton.setTitleColor(UIColor.lightGray, for: UIControl.State())
            default:
                doorButton.isEnabled = true
                longTap.isEnabled = true
                doorButton.setTitleColor(UIColor(red: 0.93, green: 0.54, blue: 0.07, alpha: 1), for: UIControl.State())
            }
        }
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setBackgroundGradient()
        setEnvironment()

        // adjust UI for iPhone4 screen
        if UIScreen.main.bounds.size.height <= 480.0 {
            doorButtonTopConstraint.constant = 20.0
        }

        // assign self to app delegate for background notifications
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.mainViewController = self

        // make button respond only to long taps to avoid accidentally opening the door
        longTap = UILongPressGestureRecognizer(target: self, action: #selector(toggleDoor(_:)))
        doorButton.addGestureRecognizer(longTap)

        resetProgressBar()

        // activate keep alive
        timer = Timer.scheduledTimer(timeInterval: checkInterval, target: self, selector: #selector(checkDoorStatus), userInfo: nil, repeats: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        infoLabel.text = unknown
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }

    func setBackgroundGradient() {
        let colors = C2AGradient()
        view.backgroundColor = UIColor.clear
        let backgroundLayer = colors.gl
        backgroundLayer.frame = view.frame
        view.layer.insertSublayer(backgroundLayer, at: 0)
    }

    // TODO setEnvironment() is obulete
    func setEnvironment() {
        let env = defaults.string(forKey: environment) ?? ""
        switch env {
        default:
            setApiClient(production)

        }

        checkDoorStatus()
    }

    func setApiClient(_ env: String) {
        infoLabel.text = unknown
        doorButton.isEnabled = false
        apiClient = C2AApiClient(environment: env, deviceName: UIDevice.current.name)
        apiClient.delegate = self
        defaults.set(env, forKey: environment)
    }

    // MARK: - Actions

    @IBAction func switchStaging(_ sender: UISwitch) {
        switch sender.isOn {
        case true:
            setApiClient(staging)
        default:
            setApiClient(production)
        }

        // print(">>> Host: \(apiClient.baseURL!)")
    }

    @IBAction func doorButtonPressed(_: C2ARoundButton) {
        lunchProgressBar(duration: 0.5)
    }

    @IBAction func doorButtonReleased(_: C2ARoundButton) {
        resetProgressBar()
    }

    @objc func toggleDoor(_ sender: UIGestureRecognizer) {
        if sender.state == .began {
            // vibrate phone
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }

        if sender.state == .ended {
//            DispatchQueue.main.async {
            apiClient.getToggleDoor(completionHandler: apiClient!.handleToggleResponse)
//            }
            sender.isEnabled = false
            doorButton.isEnabled = false
            resetProgressBar()
        }
    }

    @objc func checkDoorStatus() {
        guard UIApplication.shared.applicationState != .background else { return }

        DispatchQueue.global(qos: .background).async {
            // print("Door Status Check...")
            DispatchQueue.main.async {
                self.apiClient.getStatus(completionHandler: self.apiClient.handleStatusResponse)
            }
        }
    }

    func lunchProgressBar(duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: { () in
            self.progressBar.setProgress(1.0, animated: true)
        })
    }

    func resetProgressBar() {
        progressBar.setProgress(0.0, animated: false)
    }

    func didReceiveNotification(message: String?) {
        let alert = UIAlertController(title: "Garage Door Open", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        alert.view.tintColor = defaultColor
        present(alert, animated: true, completion: nil)
    }

    // MARK: - API Client Delegate

    func didCompleteRequest(response: String, status: Status) {
        if response == "SUCCESS" {
            serverIsAvailable = true

            switch status.status {
            case open:
                infoLabel.text = open
            case "closed":
                infoLabel.text = closed
            default:
                return
            }
        } else {
            serverIsAvailable = false
        }
    }

    func getCurrentTime() -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateStyle = .short
        formatter.timeStyle = .medium

        return formatter.string(for: Date())!
    }
}
