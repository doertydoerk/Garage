import Alamofire
import OHHTTPStubs
import OHHTTPStubsSwift
import XCTest

#if Latest
    @testable import Garage_Latest
#else
    @testable import Garage
#endif

class C2AApiClientTest: BaseTest {
    var json: [String: Any]?

    func DISABLED_testGetStatusSuccess() {
        let exp = expectation(description: "GET /status")

        _ = ["isOpen": false]

        api.getStatus(completionHandler: { response in
            self.json = response.result.value as? [String: Any]
            let statusCode = response.response?.statusCode
            XCTAssertTrue(statusCode == HTTPStatusCode.OK.rawValue, "Staus code is not 200")
            XCTAssertTrue(self.json?["isOpen"] as? Bool == false, "Key or value doesn't exist")
            exp.fulfill()
        })

        waitForExpectations(timeout: defaultTimeout, handler: { _ in
        })
    }

    func DISABLED_testPostToggle() {
        let exp = expectation(description: "POST /toogle")

        _ = ["isOpen": false]

        api.getToggleDoor(completionHandler: { response in
            self.json = response.result.value as? [String: Any]
            let statusCode = response.response?.statusCode
            XCTAssertTrue(statusCode == HTTPStatusCode.OK.rawValue, "Staus code is not 200")
            XCTAssertNotNil(self.json?["isOpen"], "JSON doesn't have key 'isOpen'")
            exp.fulfill()
        })

        waitForExpectations(timeout: defaultTimeout, handler: { _ in
        })
    }
}
