import Alamofire
@testable import Garage
import OHHTTPStubs
import XCTest

class BaseTest: XCTestCase {
    let defaultTimeout: TimeInterval = 5.0
    let api = C2AApiClient(environment: "http://lab.fritz.box:8080", deviceName: "TEST")

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
