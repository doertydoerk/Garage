//
//  C2AApiClientTest.swift
//  Garage-LatestTests
//
//  Created by Dirk Gerretz on 16.04.17.
//  Copyright © 2017 [code2app];. All rights reserved.
//

import Alamofire
import OHHTTPStubs
import XCTest

#if Latest
    @testable import Garage_Latest
#else
    @testable import Garage
#endif

class C2AApiClientTest: BaseTest {
    var json: [String: Any]?

    func DISABLED_testGetStatusSuccess() {
        let exp = expectation(description: "GET /status")

        _ = ["isOpen": false]
//        C2AServerMock.injectGetStatus(statusCode: HTTPStatusCode.OK.rawValue, jsonResponse: json)

        api.getStatus(completionHandler: { response in
            self.json = response.result.value as? [String: Any]
            let statusCode = response.response?.statusCode
            XCTAssertTrue(statusCode == HTTPStatusCode.OK.rawValue, "Staus code is not 200")
            XCTAssertTrue(self.json?["isOpen"] as? Bool == false, "Key or value doesn't exist")
            exp.fulfill()
        })

        waitForExpectations(timeout: defaultTimeout, handler: { _ in
            OHHTTPStubs.removeAllStubs()
        })
    }

    func DISABLED_testPostToggle() {
        let exp = expectation(description: "POST /toogle")

        _ = ["isOpen": false]
//        C2AServerMock.InjectPostToggle(statusCode: HTTPStatusCode.OK.rawValue, jsonResponse: json)

        api.postToggleDoor(completionHandler: { response in
            self.json = response.result.value as? [String: Any]
            let statusCode = response.response?.statusCode
            XCTAssertTrue(statusCode == HTTPStatusCode.OK.rawValue, "Staus code is not 200")
            XCTAssertNotNil(self.json?["isOpen"], "JSON doesn't have key 'isOpen'")
            exp.fulfill()
        })

        waitForExpectations(timeout: defaultTimeout, handler: { _ in
            OHHTTPStubs.removeAllStubs()
        })
    }

    func DISABLED_testPostTest() {
        let exp = expectation(description: "POST /test")

        _ = ["Notification": "sent"]
//        C2AServerMock.injectPostTest(statusCode: HTTPStatusCode.OK.rawValue, jsonResponse: json)

        api.postTest(completionHandler: { response in
            self.json = response.result.value as? [String: Any]
            let statusCode = response.response?.statusCode
            XCTAssertTrue(statusCode == HTTPStatusCode.OK.rawValue, "Staus code is not 200")
            XCTAssertTrue(self.json?["Notification"] as? String == "sent", "Key or value doesn't exist")
            exp.fulfill()
        })

        waitForExpectations(timeout: defaultTimeout, handler: { _ in
            OHHTTPStubs.removeAllStubs()
        })
    }
}
