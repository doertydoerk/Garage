//
//  ViewControllerTest.swift
//  Garage
//
//  Created by Dirk Gerretz on 19.04.17.
//  Copyright © 2017 [code2app];. All rights reserved.
//

@testable import Garage
import XCTest

class ViewControllerTest: XCTestCase {
    var sut: MainViewController!

    override func setUp() {
        super.setUp()

        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        XCTAssertNotNil(storyboard)
        sut = storyboard.instantiateViewController(withIdentifier: "MainVC") as? MainViewController
        XCTAssertNotNil(sut, "ViewController under test is nil")
        sut.viewDidLoad()
    }

    func testOutletsAvailalbe() {
        XCTAssertNotNil(sut.progressBar)
        XCTAssertNotNil(sut.infoView)
        XCTAssertNotNil(sut.doorButton)
        XCTAssertNotNil(sut.doorButtonTopConstraint)
    }
}
