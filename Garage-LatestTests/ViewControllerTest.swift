@testable import Garage
import XCTest

class ViewControllerTest: XCTestCase {
    var sut: MainViewController!

    override func setUp() {
        super.setUp()

        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        XCTAssertNotNil(storyboard)
        sut = storyboard.instantiateViewController(withIdentifier: "MainVC") as? MainViewController
        XCTAssertNotNil(sut, "ViewController under test is nil")
        sut.viewDidLoad()
    }

    func testOutletsAvailalbe() {
        XCTAssertNotNil(sut.progressBar)
        XCTAssertNotNil(sut.infoLabel)
        XCTAssertNotNil(sut.doorButton)
        XCTAssertNotNil(sut.doorButtonTopConstraint)
    }
}
