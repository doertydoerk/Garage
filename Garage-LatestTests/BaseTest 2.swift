//
//  BaseTest.swift
//  Garage
//
//  Created by Dirk Gerretz on 17.04.17.
//  Copyright © 2017 [code2app];. All rights reserved.
//

import Alamofire
@testable import Garage
import OHHTTPStubs
import XCTest

class BaseTest: XCTestCase {
    let defaultTimeout: TimeInterval = 5.0
    let api = C2AApiClient(url: "http://192.168.178.63:9000")
    let testHost = "192.168.178.63"

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCTAssertTrue(api.baseURL == "http://\(testHost):9000", "Found incorrect URL. Aborting test.\n")
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
