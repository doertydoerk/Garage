# Garage
iOS/WatchOS app to open my garage via Raspberry Pi

Features (if supported by the backend)
- switch to open close the garage door with the app or watch
- get the current door status (open/closed)
- push notification when door was opened potentially illegally

<img align="right" src="./WatchScreenShot.png" width="20%" height="20%" style="padding: 10px;"> 

Some time back I got myself a Apple Watch and I really love it. During the Christmas break I was playing around with a Raspberry Pi and a breadboard. At some point I installed an Apache server on the RasPi so I could send commands to it using HTTP requests. It wasn’t all too difficult really. So soon I was able to switch a LED on/off by just calling an URL from any browser within my LAN.
Then I figured that if I can switch an LED via the Web, why not my garage door? Because if I can do it via the HTTP request that means I could also do it from my iPhone using the browser, or I could write a very simple app…or I could even use my Apple Watch.
Actually it was a fairly simple thing to do as the motor that controls the door (an old Bosch GA Comfort 1041 at the time) already had the option to be switched by an external switch. The only thing I needed in addition to my Raspberry Pi was a 2- relay board (3 – 6€) that the Raspberry would switch instead of the LED. Actually we are only going to use one relay that is attached to the motors switch.

Since my garage is in range of my home WiFi network I don’t have a need to expose the service to the internet. Doing all this through the internet is beyond the scope of my little project. Just make sure to take every precaution to properly secure your RasPi before exposing it to the internet. Keep in mind that if you screw up somebody may open your garage from the other end of the world.
To help development I employing a second Raspberry Pi which I use as my staging environment. That way I can test out software feature or new curcuitry before 
deploying to my RasPi in the garage. This avoids e.g. unintentional opening of the garage door. The iOS app supports switch environments on the fly.

Over time I've added new featues like getting a current status of the door. For that I'm using a ultra sound sensor that reports back the doors distance from the sensor.

Find the garageAPI/backend repo [here](https://gitlab.com/doertydoerk/garage-api).


<p align="center">
    <img src="./AppScreenShot.png" width="20%" height="20%"> 
</p>